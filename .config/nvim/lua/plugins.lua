-- BOOTSTRAP LAZY.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  {
    "ribru17/bamboo.nvim",
    lazy = false,
    priority = 1000,
    config = function()
      require("bamboo").setup({
        dim_inactive = true,
        ending_tildes = true,
      })
      vim.cmd.colorscheme("bamboo")
    end,
  },
  {
    "ibhagwan/fzf-lua",
    branch = "main",
    dependencies = {
      "nvim-tree/nvim-web-devicons",
      { "junegunn/fzf", build = "./install --bin" }
    },
    config = function()
      local fzf = require("fzf-lua")
      fzf.setup({
        winopts = {
          fullscreen = true,
          preview = {
            layout = "vertical",
            vertical = "down:60%",
          },
        },
        files = {
          previewer = "builtin",
        },
      })
      fzf.register_ui_select()
    end,
  },
  {
    "echasnovski/mini.nvim",
    version = "*",
    config = function()
      require("mini.align").setup({
        mappings = {
          start = "<leader>a",
          start_with_preview = "",
        },
      })
      require("mini.files").setup()
      require("mini.move").setup()
      require("mini.splitjoin").setup()
      require("mini.surround").setup({
        mappings = {
          add = "<leader>sa",
          delete = "<leader>sd",
          replace = "<leader>sr",
          find = "",
          find_left = "",
          highlight = "",
        },
      })
      require("mini.statusline").setup()
    end,
  },
  {
    "stevearc/oil.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    opts = {
      view_options = {
        show_hidden = true,
      },
    },
  },
  {
    "jghauser/mkdir.nvim",
  },
  {
    "NvChad/nvim-colorizer.lua",
    config = true,
  },
  {
    "folke/flash.nvim",
    config = function()
      local flash = require("flash")
      flash.setup({
        modes = {
          char = {
            enabled = false,
          },
          search = {
            enabled = true,
          },
        },
      })
    end,
  },
  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    config = true,
  },
  {
    "mbbill/undotree",
  },
  {
    "neovim/nvim-lspconfig",
    dependencies = { "hrsh7th/nvim-cmp" },
    config = function()
      local lsp_capabilities = require("cmp_nvim_lsp").default_capabilities()
      -- LUA
      require("lspconfig").lua_ls.setup({
        settings = {
          Lua = {
            runtime = {
              version = "LuaJIT",
            },
            diagnostics = {
              globals = {
                "vim",
                "require",
              },
            },
            workspace = {
              library = vim.api.nvim_get_runtime_file("", true),
            },
            telemetry = {
              enable = false,
            },
          },
          capabilities = lsp_capabilities,
        },
      })
      -- GO
      require("lspconfig").gopls.setup({
        capabilities = lsp_capabilities,
        settings = {
          gopls = {
            gofumpt = true,
            usePlaceholders = true,
            analyses = {
              shadow = true,
              unusedparams = true,
              unusedvariable = true,
            },
            staticcheck = true,
          }
        },
      })
      -- PYTHON
      require("lspconfig").pyright.setup({
        capabilities = lsp_capabilities,
      })
      require("lspconfig").ruff.setup({
        -- ruff uses an LSP proxy, therefore it needs to be enabled as if it
        -- were a LSP. In practice, ruff only provides linter-like diagnostics
        -- and some code actions, and is not a full LSP yet.
        on_attach = function(client)
          -- disable ruff as hover provider to avoid conflicts with pyright
          client.server_capabilities.hoverProvider = false
        end,
      })
      -- RUST
      require("lspconfig").rust_analyzer.setup({
        capabilities = lsp_capabilities,
      })
    end
  },
  {
    "hrsh7th/nvim-cmp",
    dependencies = {
      -- sources
      "hrsh7th/cmp-nvim-lsp-signature-help",
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-cmdline",
      "FelipeLema/cmp-async-path",
      -- snippet below
      "dcampos/nvim-snippy",
      "dcampos/cmp-snippy",
    },
    config = function()
      local cmp = require("cmp")
      cmp.setup({
        snippet = {
          expand = function(args)
            require("snippy").expand_snippet(args.body)
          end,
        },
        window = {
          completion = cmp.config.window.bordered(),
          documentation = cmp.config.window.bordered(),
        },
        mapping = cmp.mapping.preset.insert({
          ["<c-u>"] = cmp.mapping.scroll_docs(-4),
          ["<c-d>"] = cmp.mapping.scroll_docs(4),
          ["<c-e>"] = cmp.mapping.abort(),
          ["<c-f>"] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
        }),
        confirmation = {
          completeopt = "menu,menuone,preview",
        },
        -- preselect = cmp.PreselectMode.None,
        sources = cmp.config.sources({
          { name = "nvim_lsp_signature_help" },
          { name = "nvim_lsp" },
          { name = "snippy" },
        }, {
          { name = "buffer" },
        })
      })
      cmp.setup.cmdline({ "/", "?" }, {
        mapping = cmp.mapping.preset.cmdline(),
        sources = {
          { name = "buffer" },
        }
      })
      cmp.setup.cmdline(":", {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources({
          { name = "async_path" },
        }, {
          { name = "cmdline" },
        })
      })
    end,
  },
  {
    "dcampos/nvim-snippy",
    config = function()
      require("snippy").setup({
        mappings = {
          is = {
            ["<c-j>"] = "expand_or_advance",
            ["<c-k>"] = "previous",
          },
        },
      })
    end,
  },
  {
    "dcampos/cmp-snippy",
  },
  {
    "fladson/vim-kitty", -- syntax highlighting for kitty config
  },
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    dependencies = { "nvim-treesitter/nvim-treesitter-textobjects" },
    config = function()
      require("nvim-treesitter.configs").setup({
        ensure_installed = {
          "c", "lua", "vim", "vimdoc", "query", "markdown", "markdown_inline", -- MUST be installed according to docs
          "comment",                                                           -- nested language, not auto associated with filetype
        },
        sync_install = false,
        auto_install = true,
        highlight = {
          enable = true,
        },
        textobjects = {
          select = {
            enable = true,
            lookahead = true,
            keymaps = {
              ["aa"] = "@parameter.outer", -- Argument
              ["ia"] = "@parameter.inner", -- Argument
              ["af"] = "@function.outer",
              ["if"] = "@function.inner",
              ["ac"] = "@comment.outer",
            },
          },
        },
      })
    end,
  },
  {
    "chrisgrieser/nvim-various-textobjs",
  },
  {
    "FabijanZulj/blame.nvim",
    config = true,
  },
  {
    "johmsalas/text-case.nvim",
    config = function()
      require("textcase").setup({
        substitude_command_name = "S",
      })
    end
  },
  {
    "gbprod/substitute.nvim",
    config = true,
  },
  {
    "sindrets/diffview.nvim",
    config = function()
      require("diffview").setup({
        view = {
          merge_tool = {
            layout = "diff4_mixed",
            disable_diagnostics = true,
          },
        },
      })
    end,
  },
})
